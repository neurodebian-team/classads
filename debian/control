Source: classads
Priority: extra
Maintainer: NeuroDebian Team <team@neuro.debian.net>
Uploaders: Michael Hanke <mih@debian.org>, Yaroslav Halchenko <debian@onerussian.com>
Build-Depends: debhelper (>= 7.0.50~), autotools-dev (>= 20100122.1~)
Standards-Version: 3.9.1
Section: libs
Homepage: http://www.cs.wisc.edu/condor/classad
Vcs-Browser: http://git.debian.org/?p=pkg-exppsy/classads.git
Vcs-Git: git://git.debian.org/git/pkg-exppsy/classads.git
XS-DM-Upload-Allowed: yes


Package: libclassad-dev
Replaces: libclassad0-dev
Conflicts: libclassad0-dev
Section: libdevel
Architecture: any
Depends: libclassad1 (= ${binary:Version}), ${misc:Depends}
Description: library for Condor's classads expression language (development)
 A classad (classified ad) is a mapping from attribute names to expressions. In
 the simplest cases, the expressions are simple constants (integer, floating
 point, or string), thus a form of property list. Attribute expressions
 can also be more complicated. There is a protocol for evaluating an attribute
 expression of a classad vis a vis another ad. Two classads match if each ad has
 attribute requirements that evaluate to true in the context of the other ad.
 Classad  matching is used by the Condor central manager to determine the
 compatibility of jobs and workstations where they may be run.
 .
 This package provides the static library and header files.


Package: libclassad1
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: library for Condor's classads expression language
 A classad (classified ad) is a mapping from attribute names to expressions. In
 the simplest cases, the expressions are simple constants (integer, floating
 point, or string), thus a form of property list. Attribute expressions
 can also be more complicated. There is a protocol for evaluating an attribute
 expression of a classad vis a vis another ad. Two classads match if each ad has
 attribute requirements that evaluate to true in the context of the other ad.
 Classad  matching is used by the Condor central manager to determine the
 compatibility of jobs and workstations where they may be run.
 .
 This package provides the runtime library.


Package: classads
Section: misc
Architecture: any
Depends: ${shlibs:Depends}, libclassad1 (= ${binary:Version}), ${misc:Depends}
Description: Condor's classad utilities
 A classad (classified ad) is a mapping from attribute names to expressions. In
 the simplest cases, the expressions are simple constants (integer, floating
 point, or string), thus a form of property list. Attribute expressions
 can also be more complicated. There is a protocol for evaluating an attribute
 expression of a classad vis a vis another ad. Two classads match if each ad has
 attribute requirements that evaluate to true in the context of the other ad.
 Classad matching is used by the Condor central manager to determine the
 compatibility of jobs and workstations where they may be run.
 .
 This package provides command line tools to manipulate, test and evaluate
 classads.
